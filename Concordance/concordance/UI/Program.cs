﻿using System;
using Analyser.Interface;
using Analyser.Implementation;
using FileProvider.Implementation;
using FileProvider.Interface;

namespace UI
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("--------Concordance--------\n" +
                "Input file name with text for analysis...");
            string fileName = Console.ReadLine();
            IAnalysator analysator = new Analysator();
            ITextFileProvider fileProvider = new TextFileProvider();
            var text = fileProvider.Read(fileName);
            foreach (var item in text)
            {
                Console.WriteLine(item);
            }
            var result = analysator.Analyze(text);
            int num = 1;
            foreach(var item in result)
            {
                Console.WriteLine($"{num}. {item.WordValue}: \t\t\t{{{item.Count}: {item.NumbersOfSentencesAsString()}}}");
                num++;
            }
            Console.ReadKey();
        }

        

    }
}
