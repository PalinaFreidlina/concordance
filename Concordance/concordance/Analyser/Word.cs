﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Analyser
{
    public class Word: IComparable<Word>
    {
        public string WordValue { get; set; }
        public int Count { get; set; } = 1;
        public List<int> NumbersOfSentance { get; set; } = new List<int>();
        public void IncCount() { Count++; }

        public int CompareTo(Word other)
        {
            return this.WordValue.CompareTo(other.WordValue);
        }
        public string NumbersOfSentencesAsString()
        {
            string result = "";
            for (int i = 0; i < NumbersOfSentance.Count; i++)
            {
                result += NumbersOfSentance[i];
                if (i != NumbersOfSentance.Count - 1)
                {
                    result += ", ";
                }
            }
            return result;
        }
    }
    
}
