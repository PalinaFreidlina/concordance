﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Analyser.Interface
{
    public interface IAnalysator
    {
        SortedSet<Word> Analyze(string[] texts);
    }
}
