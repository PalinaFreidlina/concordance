﻿using Analyser.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Analyser.Implementation
{
    public class Analysator : IAnalysator
    {
        private readonly char[] splitWords = { ' ', ',', ';', '"', '(', ')',':' };

        public SortedSet<Word> Analyze(string[] texts)
        {
            var dictionary = new SortedSet<Word>();
            string[] words = null;
            foreach (var text in texts)
            {
                var sentences = Regex.Split(text, @"[\.!\?]\s+(?=[A-Z])");
                for (int i = 0; i < sentences.Length; i++)
                {
                    words = sentences[i].Split(splitWords, StringSplitOptions.RemoveEmptyEntries);
                    foreach (var word in words)
                    {
                        bool containsWord = false;
                        foreach (var item in dictionary)
                        {
                            if (item.WordValue.Equals(word.ToLower()))
                            {
                                item.IncCount();
                                item.NumbersOfSentance.Add(i+1);
                                containsWord = true;
                                break;
                            }
                        }
                        if (!containsWord)
                        {
                            dictionary.Add(new Word { WordValue = word.ToLower(), NumbersOfSentance = new List<int>(new[]{ i+1 }) });
                        }
                    }
                }
            }
            return dictionary;
        }

        private string[] SplitForSentences(string text)
        {
            return null;
        }
    }
}
