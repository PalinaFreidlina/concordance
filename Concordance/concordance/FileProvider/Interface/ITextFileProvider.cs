﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FileProvider.Interface
{
    public interface ITextFileProvider
    {
        string[] Read(string filename);
        bool Write(string filename, string[] tests);
    }
}
