﻿using FileProvider.Interface;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FileProvider.Implementation
{
    public class TextFileProvider : ITextFileProvider
    {
        public string[] Read(string fileName)
        {
            List<string> texts = new List<string>();
            try
            {
                using (StreamReader reader = new StreamReader(File.Open(fileName, FileMode.Open)))
                {
                    while (!reader.EndOfStream)
                    {
                        texts.Add(reader.ReadLine());
                    }
                }
            }
            catch (Exception)
            {
                //logger.Error("Exception during downloading from file");
                return null;
            }
            return texts.ToArray();
        }
        
        public bool Write(string fileName, string[] texts)
        {
            try
            {
                using (StreamWriter writer = new StreamWriter(File.Open(fileName, FileMode.OpenOrCreate)))
                {
                    foreach (var text in texts)
                    {
                        writer.Write(text);
                    }
                }
                //logger.Debug("Uploading to file: " + fileName);
                return true;
            }
            catch (Exception)
            {
                //logger.Error("Exception during uploading to file");
                return false;
            }
        }
    }
}
